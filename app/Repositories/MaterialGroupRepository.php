<?php

namespace App\Repositories;

use App\Models\MaterialGroup;

class MaterialGroupRepository extends Repository
{
    /** @var MaterialGroup */
    protected $model;

    public function getAllWhichHaveMaterial()
    {
        return $this->model->has('materials')->paginate(20);
    }
}
