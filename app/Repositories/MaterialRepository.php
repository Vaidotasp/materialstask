<?php

namespace App\Repositories;

use App\Models\Material;
use App\Models\MaterialGroup;

class MaterialRepository extends Repository
{
    /** @var Material */
    protected $model;

    // Get all instances of model

    /**
     * @param int $materialGroup
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getMatterialsByGroup($materialGroup)
    {
        return $this->with('materialGroup')->where('material_group_id', $materialGroup)->paginate(50);
    }
}
