<?php

namespace App\Http\Controllers;

use App\Models\Material;
use App\Models\MaterialGroup;
use App\Repositories\MaterialGroupRepository;
use App\Repositories\MaterialRepository;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /** @var MaterialRepository */
    protected $material;

    /** @var MaterialGroupRepository */
    protected $materialGroupRepository;

    private $valdiationRules = [
        'name' => 'required|max:255',
        'material_group_id' => 'required'
    ];

    /**
     * MaterialController constructor.
     * @param Material $material
     */
    public function __construct(Material $material)
    {
        $this->material = new MaterialRepository($material);
        $this->materialGroupRepository = new MaterialGroupRepository(new MaterialGroup());
    }

    /**
     * @param int $materialGroupId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($materialGroupId)
    {
        $materials = $this->material->getMatterialsByGroup($materialGroupId);
        $materialGroup = $materialGroupId;

        return view('material.list', compact('materials', 'materialGroup'));
    }

    /**
     * @param int $materialGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($materialGroup)
    {
        $materialGroups = $this->materialGroupRepository->all();
        $materialGroupId = (int)$materialGroup;

        return view('material.create', compact('materialGroups', 'materialGroupId'));
    }

    /**
     * @param int $materialGroupId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($materialGroupId, $materialId)
    {
        $material = $this->material->show($materialId);
        $materialGroups = $this->materialGroupRepository->all();

        $materialGroupId = (int)$materialGroupId;

        return view('material.create', compact('material', 'materialGroups', 'materialGroupId'));
    }

    /**
     * @param Request $request
     * @param int $materialGroup
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $materialGroupId)
    {
        $this->validate($request, $this->valdiationRules);

        $this->material->create($request->only($this->material->getModel()->fillable));

        if (!$materialGroupId) {
            $materialGroupId = (int)$request->input('material_group_id');
        }

        return redirect()
            ->route('material.index', ['materialGroup' => $materialGroupId])
            ->with('success', 'New Material Was Added!');
    }

    /**
     * @param Request $request
     * @param $materialGroupId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $materialGroupId, $materialId)
    {
        $this->validate($request, $this->valdiationRules);

        $this->material->update($request->only($this->material->getModel()->fillable), $materialId);

        return redirect()
            ->route('material.index', ['materialGroup' => $materialGroupId])
            ->with('success', 'Material Was Updated!');
    }

    /**
     * @param int $materialGroupId
     * @param int $material
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($materialGroupId, $material)
    {
        try {
            $this->material->delete($material);
        } catch (\Exception $e) {
            return back()->with('error', 'Selected material can not be deleted!');
        }

        return redirect()
            ->route('material.index', ['materialGroup' => $materialGroupId])
            ->with('success', 'Material Was Deleted!');
    }
}
