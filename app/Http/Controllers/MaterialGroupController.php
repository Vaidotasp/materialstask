<?php

namespace App\Http\Controllers;

use App\Models\Material;
use App\Models\MaterialGroup;
use App\Repositories\MaterialGroupRepository;
use Illuminate\Http\Request;

class MaterialGroupController extends Controller
{
    /** @var MaterialGroupRepository */
    protected $materialGroup;

    private $valdiationRules = [
        'name' => 'required|max:255|unique:material_groups'
    ];

    /**
     * MaterialGroupController constructor.
     * @param MaterialGroup $materialGroup
     */
    public function __construct(MaterialGroup $materialGroup)
    {
        $this->materialGroup = new MaterialGroupRepository($materialGroup);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $materialGroups = $this->materialGroup->getAllWhichHaveMaterial();

        return view('materialGroups.list', compact('materialGroups'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('materialGroups.create');
    }

    /**
     * @param int $materialGroupId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($materialGroupId)
    {
        $materialGroup = $this->materialGroup->show($materialGroupId);

        return view('materialGroups.create', compact('materialGroup'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->valdiationRules);

        $this->materialGroup->create($request->only($this->materialGroup->getModel()->fillable));

        return redirect()->route('materialGroup.index')->with('success', 'New Material Group Was Added!');
    }

    /**
     * @param Request $request
     * @param $materialGroupId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $materialGroupId)
    {
        $this->validate($request, $this->valdiationRules);

        $this->materialGroup->update($request->only($this->materialGroup->getModel()->fillable), $materialGroupId);

        return redirect()->route('materialGroup.index')->with('success', 'Material Group Was Updated!');
    }

    /**
     * @param int $materialGroupId
     * @return int
     */
    public function destroy($materialGroupId)
    {
        try {
            $this->materialGroup->delete($materialGroupId);
        } catch (\Exception $e) {
            return back()->with('error', 'Selected material group can not be deleted!');
        }

        return redirect()->route('materialGroup.index')->with('success', 'Material Group Was Deleted!');
    }
}
