<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'materials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'material_group_id'];

    /**
     * Get the material group that owns the material.
     */
    public function materialGroup()
    {
        return $this->belongsTo('App\Models\MaterialGroup', 'material_group_id');
    }
}
