<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'material_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function materials() {
        return $this->hasMany("App\Models\Material");
    }
}
