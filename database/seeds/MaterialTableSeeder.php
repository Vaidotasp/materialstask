<?php

use App\Models\MaterialGroup as Group;
use App\Models\Material;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Material::class, 10000)->create();
    }

    /**
     * @return integer
     */
    private function getRandomMaterialGroupId() {
        $materialGroup = Group::inRandomOrder()->first();

        return $materialGroup->id;
    }
}
