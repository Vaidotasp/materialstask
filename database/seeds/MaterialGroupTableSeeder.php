<?php

use App\Models\MaterialGroup as Group;
use Illuminate\Database\Seeder;

class MaterialGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Group::class, 15)->create();
    }
}
