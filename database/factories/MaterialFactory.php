<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Material;
use App\Models\MaterialGroup as Group;
use Faker\Generator as Faker;

$factory->define(Material::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'material_group_id' => function (array $post) {
            return Group::inRandomOrder()->first()->id;
        }
    ];
});
