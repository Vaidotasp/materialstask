@extends('app')
@section('content')
	<div class="row">
		<div class="col-12 text-center mb-2">
			<h1>@if(isset($materialGroup))
					Edit
				@else
					Add New
				@endif
				Material Group</h1>
			@if($errors->has('name'))
				<div class="alert alert-danger mt-2 text-left">
					{{ $errors->first('name') }}
				</div>
			@endif
		</div>
		<div class="col-6">
			@if(isset($materialGroup))
				{!! Form::model($materialGroup, ['route' => ['materialGroup.update', $materialGroup->id], 'method' => 'patch']) !!}
			@else
				{!! Form::open(['route' => 'materialGroup.store']) !!}
			@endif
				<div class="form-group">
					<label for="name">Material Group</label>
					{!! Form::text('name',null,[
						'class' => 'form-control',
						'id' => 'name',
						'placeholder' => 'Material Group',
						'required'
					]) !!}
				</div>
			<div class="form-group">
				{!! Form::submit('Submit', [
					'class' => 'btn btn-outline-primary' ,
					'title' => 'Add',
				]) !!}
			{!! Form::close() !!}
			</div>
		</div>
	</div>
	@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
	@endif
@endsection