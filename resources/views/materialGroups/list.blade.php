@extends('app')
@section('content')
	@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
	@endif
	@if(session('error'))
		<div class="alert alert-danger mt-2 text-left">
			{{ session('error') }}
		</div>
	@endif
	<div class="row">
		<div class="col-12">
			<a href="{{ route('material.create', ['materialGroup' => 0]) }}" class="btn btn-outline-secondary float-right  ml-2"> Add
				Material</a>
			<a href="{{ route('materialGroup.create') }}" class="btn btn-outline-primary float-right"> Add new Group</a>
		</div>
	</div>
	<div class="row">
		<div class="col-12 mb-2 text-center">
			<h1>Material Groups List</h1>
		</div>
		<div class="col-12">
			<table class="table table-striped">
				<thead class="thead-dark">
				<tr>
					<th scope="col" style="width: 1%" class="text-center">#</th>
					<th scope="col" style="width: 70%" class="text-left">Group Name</th>
					<th scope="col" class="text-right">Actions</th>
				</tr>
				</thead>
				<tbody>
					@foreach($materialGroups as $materialGroup)
						<tr>
							<td scope="row" class="text-center">{{ $materialGroup->id }}</td>
							<td class="text-left">{{ $materialGroup->name }}</td>
							<td class="text-right">
								{!! Form::open(['route' => ['materialGroup.destroy', $materialGroup->id], 'method' => 'delete']) !!}
									<div class='btn-group'>
										<a href="{!! route('material.index', ['materialGroup' => $materialGroup->id]) !!}" class='btn btn-outline-secondary mr-2'>Materials</a>
										<a href="{!! route('materialGroup.edit', [$materialGroup->id]) !!}" class='btn btn-outline-primary mr-2'>Edit</a>
										{!! Form::button('Delete', [
											'type' => 'submit',
											'class' => 'btn btn-outline-danger',
											'onclick' => "return confirm('Delete Material Group?')"
										]) !!}
									</div>
								{!! Form::close() !!}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $materialGroups->links() }}
		</div>
	</div>
@endsection