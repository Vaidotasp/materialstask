<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">

    <title>Material Practice Task</title>
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" type="text/css">
    @yield('css')
</head>
<body class="mt-4">

<div class="container">
    @yield('content')
</div>

<script src="{{ URL::asset('js/app.js') }}"></script>
@yield('javascript')

</body>
</html>