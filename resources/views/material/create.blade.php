@extends('app')
@section('content')
	<div class="row">
		<div class="col-12 text-center mb-2">
			<h1>@if(isset($material))
					Edit
				@else
					Add New
				@endif
				Material</h1>
			@if($errors->has('name'))
				<div class="alert alert-danger mt-2 text-left">
					{{ $errors->first('name') }}
				</div>
			@endif
		</div>
		<div class="col-6">
			@if(isset($material))
				{!! Form::model($material, ['route' => ['material.update', $materialGroupId, $material->id], 'method' => 'patch']) !!}
			@else
				{!! Form::open(['route' => ['material.store', $materialGroupId]]) !!}
			@endif
				<div class="form-group">
					<label for="name">Material</label>
					{!! Form::text('name',null,[
						'class' => 'form-control',
						'id' => 'name',
						'placeholder' => 'Material',
						'required'
					]) !!}
				</div>
				<div class="form-group">
					<select name="material_group_id" class="form-control" required>
						@foreach($materialGroups as $materialGroup)
							@if ($materialGroupId == $materialGroup->id)
								<option value="{{ $materialGroup->id }}" selected>{{ $materialGroup->name }}</option>
							@else
								<option value="{{ $materialGroup->id }}">{{ $materialGroup->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			<div class="form-group">
				{!! Form::submit('Submit', [
					'class' => 'btn btn-outline-primary' ,
					'title' => 'Add',
				]) !!}
			{!! Form::close() !!}
			</div>
		</div>
	</div>
	@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
	@endif
@endsection