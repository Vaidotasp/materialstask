@extends('app')
@section('content')
	@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
	@endif
	@if(session('error'))
		<div class="alert alert-danger mt-2 text-left">
			{{ session('error') }}
		</div>
	@endif
	<div class="row">
		<div class="col-12">
			<a href="{{ route('material.create', ['materialGroup' => $materialGroup]) }}" class="btn btn-outline-primary float-right"> Add new Material</a>
		</div>
	</div>
	<div class="row">
		<div class="col-12 mb-2 text-center">
			<h1>Material List</h1>
		</div>
		<div class="col-12">
			<table class="table table-striped">
				<thead class="thead-dark">
				<tr>
					<th scope="col" style="width: 1%" class="text-center">#</th>
					<th scope="col" style="width: 35%" class="text-left">Material Name</th>
					<th scope="col" style="width: 35%" class="text-left">Material Group</th>
					<th scope="col" class="text-right">Actions</th>
				</tr>
				</thead>
				<tbody>
					@foreach($materials as $material)
						<tr>
							<td scope="row" class="text-center">{{ $material->id }}</td>
							<td class="text-left">{{ $material->name }}</td>
							<td class="text-left">{{ $material->materialGroup->name }}</td>
							<td class="text-right">
								{!! Form::open(['route' => ['material.destroy', $materialGroup, $material->id],'method' => 'delete']) !!}
									<div class='btn-group'>
										<a href="{!! route('material.edit', [$materialGroup, $material->id]) !!}" class='btn btn-outline-primary mr-2'>Edit</a>
										{!! Form::button('Delete', [
											'type' => 'submit',
											'class' => 'btn btn-outline-danger',
											'onclick' => "return confirm('Delete Material?')"
										]) !!}
									</div>
								{!! Form::close() !!}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $materials->links() }}
		</div>
	</div>
@endsection