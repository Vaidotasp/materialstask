<?php

namespace Tests\Feature;

use App\Models\Material;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MaterialTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMaterialGroupCreation()
    {
        factory(Material::class)->create([
            'name' => 'Material1',
            'material_group_id' => 1,
        ]);

        $this->assertDatabaseHas('materials', ['name' => 'Material1', 'material_group_id' => 1]);
    }
}
