<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\MaterialGroup as Group;

class MaterialGroupTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMaterialGroupCreation()
    {
        factory(Group::class)->create([
            'name' => 'MaterialGroup1',
        ]);

        $this->assertDatabaseHas('material_groups', ['name' => 'MaterialGroup1']);
    }
}
