<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MaterialGroupController@index');

Route::resource('materialGroup', 'MaterialGroupController');

//Matterial routes
Route::get('/material/{materialGroup}', 'MaterialController@index')->name('material.index');
Route::delete('/material/{materialGroup}/{material}', 'MaterialController@destroy')->name('material.destroy');
Route::get('/material/{materialGroup}/create', 'MaterialController@create')->name('material.create');
Route::post('/material/{material}', 'MaterialController@store')->name('material.store');
Route::get('/material/{materialGroup}/{material}/edit', 'MaterialController@edit')->name('material.edit');
Route::patch('/material/{materialGroup}/{material}', 'MaterialController@update')->name('material.update');
//Route::resource('material', 'MaterialController');